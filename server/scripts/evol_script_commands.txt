---------------------------------------
//=====================================
99 - Evol Script Commands
//=====================================
---------------------------------------

*chatjoin chatId [, char [,password]];

Force player to join chat with id chatId.

Example:
    chatjoin .chat;

---------------------------------------

*setcamnpc [npcName [, x, y]];

Enable camera moving mode in client and move camera to npc with name "npcName"
and move relative from it in x and y pixels.
If "npcName" is missing, using attached npc.
If x and y missing, it count as 0.

Example:
    setcamnpc 32, 0;
    setcamnpc "npc1";

---------------------------------------

*setcam x, y;

Move camera to absolute position x,y in pixels.

Example:
    setcam 0, 0;
    setcam 2000, 512;

---------------------------------------

*movecam x, y;

Enable camera moving mode in client and move camera relative to player
position by x and y pixels.

Example:
    movecam 10, 20;

---------------------------------------

*restorecam;

Disable camera moving mode in client and set camera to default position.

Example:
    restorecam;

---------------------------------------

*npctalk3 text;

Send chat message from current npc, visible only for attached player.
Npc name will be translated.

Example:
    npctalk3 "hello";

---------------------------------------

*closedialog;

Will close npc dialog in client and partially in server side.
If npc script will continue after this command, dialog can be opened again,
but not all actions may works.
Player must be attached.

Example:
    closedialog;

---------------------------------------

*closeclientdialog;

Will close npc dialog in client only.
If npc script will continue after this command, dialog can be opened again.
Player must be attached.

Example:
    closeclientdialog;

---------------------------------------

*clear;

Clear from text npc dialog for attached npc.
Player must be attached.

Example:
    clear;

---------------------------------------

*shop npcName;

Open if exists for attached player npc shop from npc with name "npcName"

Example:
    shop "bag";

---------------------------------------

*getitemlink itemName [, cardName1 [, cardName2 [, cardName3 [, cardName4]]]];
*getitemlink itemId [, cardName1 [, cardName2 [, cardName3 [, cardName4]]]];

Return link for item id "itemId" or name "itemName" with cards.
This link after can be used in mes or other commands what show messages in npc dialog.

Example:
    mes "Acorn link: " + getitemlink(Acorn);
    mes getitemlink(VneckJumper, MintCashmereDye);

---------------------------------------

*getinvindexlink index;

Return link to item by player inventory index. Also return cards in link.
This link after can be used in mes or other commands what show messages in npc dialog.
Inventory index counted from 0.

Example:
    mes "link here: " + getinvindexlink(3);

---------------------------------------

*l text [, param1, ...];

Return translated formatted string.
Player must be attached.

Example:
    mes l("test line");
    mes l("give me @@.", getitemlink(Acorn));

---------------------------------------

*lg femaleText, makeText, [, param1, ...];
*lg text;

Return translated formatted string based on gender.
Player must be attached.
Translators will give text with #N.

Example:
    // translators will get two strings "i went to shop.#0" and "i went to shop.#1"
    // with #0 for female and #1 for male
    lg("i went to shop.");
    lg("call her @@.", "call him @@.", "test");

---------------------------------------

*requestlang;

Return selected language in client to server.
Player must be attached.

Example:
    .@lang = requestlang();

---------------------------------------

*requestitem;

Return selected item id by player. In client player move this item to npc.
Player must be attached.

Example:
    .@item = requestitem();
    mes "You gave me " + getitemlink(.@item);

---------------------------------------

*requestitems [n];

Return n items from player what he moved to npc inventory separated by ";".
If n missing it mean 1.
Better not use this command. Better use requestcraft.
Player must be attached.

Example:
    .@str$ = requestitems();
    .@str$ = requestitems(3);

---------------------------------------

*requestitemindex;

Return inventory index for selected item by player. In client player move this item to npc.
Player must be attached.

Example:
    .@item = requestitemindex();

---------------------------------------

*requestitemsindex [n];

Return inventory indexes for n items from player what he moved to npc inventory separated by ";".
If n missing it mean 1.
Player must be attached.

Example:
    .@str$ = requestitemsindex();
    .@str$ = requestitemsindex(3);

---------------------------------------

*requestcraft [n];

Request selected craft items in string format.
n is craft inventory size. If n missing, it mean 1.
After this string must be validated and converted to craft object
Player must be attached.

Example:
    .@str$ = requestcraft(9);

---------------------------------------

*initcraft str;

Create craft object based on craft string sent from client.
Return craft object id. Or -1 on error.
After using, craft object must be deleted with function deletecraft.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    deletecraft(.@craft);

---------------------------------------

*dumpcraft id;

Dump to server console given craft object with id 'id'.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    dumpcraft(.@craft);
    deletecraft(.@craft);

---------------------------------------

*deletecraft id;

Delete craft object.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    deletecraft(.@craft);

---------------------------------------

*findcraftentry id, flag;

Search craft entry in craft db for craft object with id 'id' and flag 'flag'.
Retrun craft entry id what was found. If error happend, return -1.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    .@entry = findcraftentry(.@craft, 0);
    deletecraft(.@craft);

---------------------------------------

*usecraft id;

This command can be used after checked craft object and found craft entry.
It apply craft entry. May delete or give items and do other things.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    .@entry = findcraftentry(.@craft, 0);
    usecraft .@craft;
    deletecraft(.@craft);

---------------------------------------

*getcraftcode id;

After craft object was used by function usecraft, this function based on craft entry id
can return special number configured for craft entry in craft db.
Based on this value script may do additional things.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    .@entry = findcraftentry(.@craft, 0);
    usecraft .@craft;
    .@code = getcraftcode(.@entry);
    deletecraft(.@craft);

---------------------------------------

*getcraftslotid id, slot;

Return item id selected in craft object 'id' and in slot 'slot'.
On error return 0.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    .@item = getcraftslotid(.@craft, 0);
    deletecraft(.@craft);

---------------------------------------

*getcraftslotamount id, slot;

Return amount selected in craft object 'id' and in slot 'slot'.
On error return 0.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    .@item = getcraftslotid(.@craft, 0);
    .@amount = getcraftslotamount(.@craft, 0);
    deletecraft(.@craft);

---------------------------------------

*validatecraft id;

Validate craft object and current player inventory.
It can be used after "next" command between init and use craft.
Return 0 if craft object now invalid, 1 if craft object can be used for player.
Player must be attached.

Example:
    .@str$ = requestcraft(9);
    .@craft = initcraft(.@str$);
    next;
    if (valudatecraft(.@craft) == 0)
    {
        deletecraft(.@craft);
        close;
    }
    .@item = getcraftslotid(.@craft, 1);
    deletecraft(.@craft);

---------------------------------------

*getq quest;

Return quest state (field 1) for attached player.

Example:
    mes getq(ShipQuests_Julia);

---------------------------------------

*getq2 quest;

Return quest field 2 for attached player.

Example:
    mes getq2(ShipQuests_Julia);

---------------------------------------

*getq3 quest;

Return quest field 3 for attached player.

Example:
    mes getq3(ShipQuests_Julia);

---------------------------------------

*getqtime quest;

Return quest time for attached player.

Example:
    mes getqtime(ShipQuests_Julia);

---------------------------------------

*setq quest, value1 [, value2 [, value3 [, time]]];

Set quest state and fields for attached player.

Example:
    setq ShipQuests_Julia, 1;
    setq ShipQuests_Julia, 1, 2;
    setq ShipQuests_Julia, 1, 2, 3, 100;

---------------------------------------

*setnpcdir [npc,] dir;

Set direction 'dir' for npc with name 'npc' or attached npc.

Example:
    setnpcdir DOWN;
    setnpcdir "npc1", LEFT;

---------------------------------------

*npcsit [name];

Set npc it sit state. 'name' is npc name. If 'name' missing it using attached npc.

Example:
    npcsit;
    npcsit "alige";

---------------------------------------

*npcstand [name];

Set npc it stand state. 'name' is npc name. If 'name' missing it using attached npc.

Example:
    npcstand;
    npcstand "alige";

---------------------------------------

*setnpcsex npcName, gender;

Set gender 'gender' for npc 'npcName'
Supported genders:
    G_FEMALE - female
    G_MALE   - male
    G_OTHER  - no gender or other

Example:
    setnpcsex "npc1", G_MALE;

---------------------------------------

*npcwalkto x, y;

This command start walking attached npc to position x,y.

Example:
    npcwalkto 10, 10;

---------------------------------------

*setnpcdialogtitle title;

Set dialog title for attached npc to 'title'.
Player must be attached.

Example:
    setnpcdialogtitle "Hello";

---------------------------------------

*rif condition, trueValue [, falseValue];

Check condition and if it true, then return 'trueValue' string
If condition false, it return 'falseValue' if present, or empty string.
This command can be used in menu and select commands. Empty lines in this commands hidden.

Example:
    .@ret = select(rif(countitem(Acorn) > 0, "I can give you acorn."),
        "Nothing");

---------------------------------------

*setmapmask mapName, mask;

Allow change map mask. Based on this mask layers in client can be visible or hidden.
Command 'setmapmask' set mask for 'mapName' to value 'mask'.
Default mask is 1.

Example:
    setmapmask "test", 1;  // default
    setmapmask "test", 3;  // 1 + 2

---------------------------------------

*addmapmask mapName, mask;

Allow change map mask. Based on this mask layers in client can be visible or hidden.
Command 'addmapmask' add bit mask 'mask' to map 'mapName'.
Default mask is 1.

Example:
    setmapmask "test", 1;  // now mask 1
    addmapmask "test", 2;  // now mask is 1|2 = 3
    addmapmask "test", 2;  // now mask is 3|2 = 3

---------------------------------------

*removemapmask mapName, mask;

Allow change map mask. Based on this mask layers in client can be visible or hidden.
Command 'removemapmask' remove bit mask 'mask' from map 'mapName'.
Default mask is 1.

Example:
    setmapmask "test", 1;  // now mask 1
    addmapmask "test", 2;  // now mask is 1|2 = 3
    removemapmask "test", 1;  // now mask is 3|1^1 = 2
    removemapmask "test", 1;  // now mask is 2|1^1 = 2

---------------------------------------

*getmapmask mapName;

Return current mask what was set to map 'mapName'.

Example:
    mes "test map mask: " + getmapmask("test");

---------------------------------------

*sendmapmask mask{, "player name"};

Send map mask for a specific player. Changes will not persist for long.

---------------------------------------

*showavatar [id];

Show avatar in npc dialog for attached npc.
if 'id' misisng or 0 it hide avatar.
Player must be attached.

Example:
    showavatar 1;  // show 1
    showavatar;  // hide

---------------------------------------

*setavatardir dir;

Set avatar direction 'dir' in npc dialog for attached npc.
Player must be attached.

Example:
    setavatardir UPRIGHT;

---------------------------------------

*setavataraction action;

Set avatar action to 'action' in npc dialog for attached npc.
Player must be attached.

Example:
    setavataraction ACTION_SIT;

---------------------------------------

*changemusic map, file;

Change background music on map 'map' to file 'file'.

Example:
    changemusic "music1.ogg";

---------------------------------------

*getmapname;

Return attached player current map name.

Example:
    mes "You located in map: " + getmapname();

---------------------------------------

*unequipbyid id;

Unequip from attached player item with id 'id'.

Example:
    unequipbyid VneckJumper;

---------------------------------------

*ispcdead(nickname);

Return true if attached player is dead. In other cases return false.

---------------------------------------

*getareadropitem mapName, x1, y1, x2, y2, itemId [, delFlag];
*getareadropitem mapName, x1, y1, x2, y2, itemName [, delFlag];

Return number of floor items with id 'itemId' or name 'itemName' in map 'mapName'
in rectangle (x1,y1) - (x2,y2). If delFlag set to 1, it also delete floor items.
This function probably will be removed in future.

Example:
    mes "Acorns amount: " + getareadropitem("test", 10, 10, 20, 20, Acorn);

---------------------------------------

*clientcommand command;

Send client side command to client. It allow send only safe commands.
Player must be attached.

Example:
    clientcommand "emote 1";  // show first emote on attached player

---------------------------------------

*isunitwalking [unitId];

Return true if unit with id 'unitId' in walking progress.
If 'unitId' missing it using attached npc.
This function can be used in walking npc scripts.

Example:
    mes "Current npc walking? " + (isunitwalking() ? "yes" : "no");

---------------------------------------

*failedrefindex index;

This function do fail refine action on item with inventory index 'index'.
Index start count from 0.
Player must be attached.

Example:
    failedrefindex 3;

---------------------------------------

*downrefindex index, levels;

This function lower refine item level with inventory index 'index' for 'levels' levels.
Index start count from 0.
Player must be attached.

Example:
    downrefindex 3, 1;

---------------------------------------

*successrefindex index, levels;

This function up refine item level with inventory index 'index' for 'levels' levels.
Index start count from 0.
Player must be attached.

Example:
    successrefindex 3, 1;

---------------------------------------

*setbgteam bgId, teamId;

Set id 'teamId' for battle ground group 'bgId'.
After battle ground start, teamId will be sent to client.

Example:
    $@bgid1 = waitingroom2bg("testbg", 10, 10, "bgnpc1::OnLogout","bgnpc1:OnDie");
    setbgteam $@bgid1, 1;
    bg_warp $@bgid1, "testbg", 10, 10;

---------------------------------------

*checknpccell;

Check given cell in map for flag for attached npc.
Most time it used for check is this cell walkable for npc or not.

Example:
    .@canWalk = checknpccell("test", 10, 10, cell_chkpass);

---------------------------------------

*setcells mapName, x1, y1, x2, y2, mask, wallName;

Add to map with name 'mapName' new "wall" in rectangle (x1,y1) - (x2,y2).
Set to this rectangle mask 'mask'. 'wallName' is name for this "wall".
Mask is client side collision types.
After it can be removed by this name.

Example:
    setcells "test", 14, 11, 17, 11, 3, "wall1";

Masks:                          Blockmask
0 - COLLISION_EMPTY             16
1 - COLLISION_WALL              128
2 - COLLISION_AIR               4
3 - COLLISION_WATER             8
4 - COLLISION_GROUNDTOP         32
5 - COLLISION_PLAYER_WALL       64
6 - COLLISION_MONSTER_WALL      2

---------------------------------------

*delcells wallName;

Remove wall created by command setcells.

Example:
    setcells "test", 14, 11, 17, 11, 3, "wall1";
    delcells "wall1";

---------------------------------------

*setmount id;

Set mount (horse) for attached player.
If id is zero, mount removed.

Example:
    setmount 1;

---------------------------------------

*setskin name;

Set skin with name 'name' for attached npc for attached player.

Example:
    setskin "test2";

---------------------------------------

*emotion id [, flag [, name]];

This command works same like hercules emotion command, except it removed limit for emotes.

---------------------------------------

*successremovecardsindex index;

This command remove all cards from item by inventory index and put all cards in inventory.
Also show status effect 3.

---------------------------------------

*failedremovecardsindex index, flag;

This command remove all cards from item by inventory index.
Also show status effect 2.
It also may remove item or cards depend on flag.
flag value:
    0 - will remove item and cards.
    1 - will remove cards and keep item.
    2 - will remove item and keep cards.
    3 - do nothing except status effect.

---------------------------------------

*getcardbyindex itemIndex, cardIndex;

This command return card id by item index and card (slot) index.
If no cards found or error happend, return zero.

Example:
    .@item = requestitemindex();
    mes "slot 0 = card " + str(getcardbyindex(.@item, 0));

---------------------------------------

*removecardbyindex itemIndex, cardIndex;

This command remove card from invetory and slot index.
If error happend, return -1.
If no errors, return 0.

Example:
    .@item = requestitemindex();
    removecardbyindex .@item, 0;

---------------------------------------

*htnew;

Create a new hash table (hash map, associative array), and return it's ID.
All hashtable functions (htdelete, htget, htput, htclear, htsize, htiterator)
will use this ID (as first argument). Remember to use htdelete to free memory,
when you don't need the hash table anymore.

Example:
    .@ht_id = htnew;

---------------------------------------

*htdelete id;

Delete a hashtable with given ID, and free the memory.

Example:
    .@ht_id = htnew;
    htdelete(.@ht_id);

---------------------------------------

*htget id, key [, default];

Return the value, associated with the given key. If there is no such value,
return 0. If the (optional) third argument is given, and no value was found,
return this third argument.

Example:
    .@val1 = htget(.@ht_id, "key1");
    .@val2 = htget(.@ht_id, "key2", 7);
    .@val3$ = htget(.@ht_id, "key3", "");

---------------------------------------

*htput id, key, newval;

Set a new value, associated with given key. If a previous value existed,
it will be replaced. If newval is empty string or 0, the given key and
it's associated value are removed.

Example:
    htput(.@ht_id, "key1", 77);
    htput(.@ht_id, "key1", "test");
    htput(.@ht_id, "key1", 0);      // delete given entry from hashtable

---------------------------------------

*htsize id;

Get the number of elements in the given hash table. When you set a given
value to 0 or "", it's removed from the hash table, so it won't count when
calculating the size.

Example:
    .@len = htsize .@ht_id;

---------------------------------------

*htexists id;

Check is given hash table exists.
Return false if hash table not exists.
Return true if hash table exists.

Example:
    .@exists = htexists .@ht_id;

---------------------------------------

*htclear id;

Remove all elements (keys and values) from the given hash table. After it
it's size will be 0.

Example:
    htclear(.@ht_id);

---------------------------------------

*htiterator id;

Create an iterator over a hash table keys, and return it's ID (don't confuse
iterator ID with hash table ID, those are different). Iterators are used
to traverse over hash tables, get keys and possibly modify values. Remember
to use htidelete to remove the iterator and free the resources.

Example: see below.

---------------------------------------

*htinextkey it;

Get the next key of the hash table, that the given iterator is attached to.
If the iterator traversed over all elements, return "".

Example: see below.

---------------------------------------

*hticheck it;

Check if the iterator traversed over all keys. Returns 1, if it didn't,
0 otherwise.

Example: see below

---------------------------------------

*htidelete it;

Delete the iterator and free resources.

Example:

    .@it = htiterator(.@ht_id);
    for (.@key$ = htinextkey(.@it); hticheck(.@it); .@key$ = htinextkey(.@it))
    {
        .@oldval = htget(.@ht_id, .@key$);
        htput(.@ht_id, .@key$, .@oldval + .@oldval);  // concatenate each value with itself
    }
    htidelete(.@it)

---------------------------------------

*setfakecells x1, y1 [, x2, y2 ], mask;

Send fake collision data to the client. Sets the cells of square
area 'x1', 'y1' [, 'x2', 'y2'] to BlockType 'mask'. Only exists client-side.
For real collision, use the setcells & delcells commands.

Example:
    setfakecells 25, 45, 29, 50, 1; // block
    setfakecells 25, 45, 29, 50, 0; // allow

---------------------------------------

*getlabel label;

Get label and return int.

Example:
    .@var = getlabel(OnInit);

---------------------------------------

*tolabel num;

Get number and return label.

Example:
    goto tolabel(.@var);

---------------------------------------

*slide x, y;

Moves a player within the same map.
If warping players on a map where they already are, you should always prefer
slide over warp, as warp makes the client reload and re-render the whole map
while slide just tells the client to reposition the character.

Example:
    slide 25, 194;

---------------------------------------

*getitemoptionidbyindex invIndex, optIndex;

Return option id for item with inventory index invIndex and option index optIndex.

Example:
    mes getitemoptionidbyindex(10, 0);
    mes getitemoptionidbyindex(10, 1);
    mes getitemoptionidbyindex(10, 2);
    mes getitemoptionidbyindex(10, 3);
    mes getitemoptionidbyindex(10, 4);

---------------------------------------

*getitemoptionvaluebyindex invIndex, optIndex;

Return option value for item with inventory index invIndex and option index optIndex.

Example:
    mes getitemoptionvaluebyindex(10, 0);
    mes getitemoptionvaluebyindex(10, 1);
    mes getitemoptionvaluebyindex(10, 2);
    mes getitemoptionvaluebyindex(10, 3);
    mes getitemoptionvaluebyindex(10, 4);

---------------------------------------

*getitemoptionparambyindex invIndex, optIndex;

Return option parameter for item with inventory index invIndex and option index optIndex.
For now item parameters not saved and useless.

Example:
    mes getitemoptionparambyindex(10, 0);
    mes getitemoptionparambyindex(10, 1);
    mes getitemoptionparambyindex(10, 2);
    mes getitemoptionparambyindex(10, 3);
    mes getitemoptionparambyindex(10, 4);

---------------------------------------

*setitemoptionbyindex invIndex, optIndex, [optName,] optValue;

Set option value and type for item with inventory index invIndex and option index optIndex.
optName is item option name field from item_options.conf.
optValue is any custom data for item option with given name.

Example:
    // .@item here is item inventory index
    setitemoptionbyindex(.@item, 0, VAR_MAXHPAMOUNT, 200);
    setitemoptionbyindex(.@item, 1, VAR_STRAMOUNT, 10);
    setitemoptionbyindex(.@item, 2, VAR_VITAMOUNT, -5);

---------------------------------------

*isinstance id;

Return true if id is correct instance id.
Return false if id is not instance id.

Example:
    .instid = 1;
    mes(str(isinstance(.instid));

---------------------------------------

*getnpcsubtype {"npc name"};

Undocumented function. Returns nd->subtype

---------------------------------------

*readbattleparam accountId, data;

return the battle stats of a structure. Supported values are most of UDT_* ones
Not all values are supported; Only those without another assessor are (eg. MaxHP
vs UDT_MAXHP)

---------------------------------------

*kick "player name"{, reason};
*kick accountId{, reason};

Kicks player with reason. Defaults to 15 = disconnection forced by a GM
Reason 7 is reserved for the Mirror Lake Protocol (aka. Intergame protocol).
See https://github.com/ManaPlus/ManaPlus/blob/master/src/net/eathena/generalrecv.cpp#L48 for list of reasons.






---------------------------------------
//=====================================
// TMW2 Custom Script Commands
//=====================================
---------------------------------------

*getguildinfo(<guild id>);

Records in mapreg info about a guild. The array Index is the Guild ID.
This command is meant to be used in GvG setup, where you might need the data from
several guilds at once and it is better to have them saved to memory.

Upon executing this,

$@guildinfo_lvl[]    is a global temporary array which contains the guild level

$@guildinfo_avg[]    is a global temporary array which contains the average player
                     level within a guild.

$@guildinfo_exp[]    says the current guild EXP. It's a hack and will be removed.

$@guildinfo_nxp[]    says the EXP needed to lvl up. It's a hack and will be removed.

Returns 0 on failure.

---------------------------------------
*getguildlvl(<guild id>);
*getguildavg(<guild id>);
*getguildexp(<guild id>);
*getguildnxp(<guild id>);

Basically the same as getguildinfo(), but doesn't saves to mapreg, instead,
returns the value. This is meant to be the most widely used by NPC scripts.

Returns -1 on failure.

---------------------------------------

*setguildrole(guild id, gpos id, gperm id, exp tax, role name);

This does what ManaPlus will eventually feature, allowing to change a guild role.
All parameters are required.
---------------------------------------

*getguildmember(<guild id>{, <type>});

This command will find all members of a specified guild and returns their names
(or character id or account id depending on the value of "type") into an array
of temporary global variables.

Upon executing this,

$@guildmembername$[] is a global temporary string array which contains all the
                     names of these guild members.
                     (only set when type is 0 or not specified)

$@guildmembercid[]   is a global temporary number array which contains the
                     character id of these guild members.
                     (only set when type is 1)

$@guildmemberaid[]   is a global temporary number array which contains the
                     account id of these guild members.
                     (only set when type is 2)

$@guildmembercount   is the number of guild members that were found.

$@guildmemberpos[]   is the position ID of every member found.

The guild members will be found regardless of whether they are online or offline.
Note that the names come in no particular order.

Be sure to use $@guildmembercount to go through this array, and not
getarraysize(), because it is not cleared between runs of getguildmember().

For usage examples, see getpartymember().

---------------------------------------
*gethomunexp(exp)

Give [exp] to homunculus. Homunculus must be alive and invoked.
It'll fail silently otherwise.

---------------------------------------
*deployhomunculus()
*recallhomunculus()

Send or Recall Homunculus from a mission. The functions only prevents the
Homunculus from being summoned during the mission. No timer is run on the
meanwhile - they won't hunger, and calling recallhomunculus() is the scriptwriter
job.

---------------------------------------
*homstatus()

Returns the homunculus status.

0: Homunculus is active
1: Homunculus is sleeping
2: Homunculus is on a mission

---------------------------------------
*gethomunid()

Returns attached player's homunculus' GID.
Compatible with getunitdata/setunitdata.

---------------------------------------
*sethomunclass(new_class)

Permanently modify attached player's homunculus' class to <new_class>.
Homunculus must be active and alive. Stats will NOT be updated.

0 = success
1 = no homunculus found
2 = homunculus dead or resting
3 = invalid arguments
4 = Failed to change class

---------------------------------------
*sethomunlevel(new_level)

Permanently modify attached player's homunculus' level to <new_level>.
Shuffle homunculus status afterwards. Should be run after sethomunclass.
Does not return anything.

---------------------------------------
*readparam2(param{, player name or id})

Same as readparam but considers bonuses as well, whereas readparam reads base.
Deprecated: Use getunitdata() instead.

Supported values: bMaxHP, bMaxSP, bStr, bAgi, bVit, bInt, bDex, bLuk, bAtk, bDef

---------------------------------------
*debugmes("<format string>"{, <param>{, ...}})

This command will print debug message in the server console (map-server window),
after applying the same format-string replacements as sprintf(). It will not be
displayed anywhere else, but may be logged. Returns true on success.

IMPORTANT: Do not use this command with unsanitized player input.

---------------------------------------
*consolewarn("<format string>"{, <param>{, ...}})

This command will print a warning in the server console (map-server window),
after applying the same format-string replacements as sprintf(). It will not be
displayed anywhere else, but will be logged. Returns true on success.

IMPORTANT: Do not use this command with unsanitized player input.

---------------------------------------
*consolebug("<format string>"{, <param>{, ...}})

This command will print an error in the server console (map-server window),
after applying the same format-string replacements as sprintf(). It will not be
displayed anywhere else, but will be logged. Returns true on success.

IMPORTANT: Do not use this command with unsanitized player input.

---------------------------------------
*consoleinfo("<format string>"{, <param>{, ...}})

This command will print a notice in the server console (map-server window),
after applying the same format-string replacements as sprintf(). It will not be
displayed anywhere else, but may be logged. Returns true on success.

IMPORTANT: Do not use this command with unsanitized player input.

---------------------------------------

*atcommand("<command>")

This command will run the given command line exactly as if it was typed in
from the keyboard by the player connected to the invoking character, and
that character belonged to an account which had GM level 99. Note that it does
not grant actual GM level 99 to the account.

---------------------------------------

*countitem(<item id>{, accountId})
*countitem("<item name>"{, accountId})

This function will return the number of items for the specified item ID
that the invoking character has in the inventory. While item name can be used,
its use is deprecated.

---------------------------------------

*getinventorylist()

This command sets a bunch of arrays with a complete list of whatever the
invoking character has in its inventory, including all the data needed to
recreate these items perfectly if they are destroyed. Here's what you get:

@inventorylist_id[]        - array of item ids.
@inventorylist_amount[]    - their corresponding item amounts.
@inventorylist_equip[]     - will return the slot the item is equipped on, if at all.
@inventorylist_refine[]    - for how much it is refined.
@inventorylist_identify[]  - whether it is identified.
@inventorylist_attribute[] - whether it is broken.
@inventorylist_card1[]     - These four arrays contain card data for the
@inventorylist_card2[]       items. These data slots are also used to store
@inventorylist_card3[]       names inscribed on the items, so you can
@inventorylist_card4[]       explicitly check if the character owns an item
                             made by a specific craftsman.
@inventorylist_opt_id1[]   - These five arrays contain option ID data for items.
@inventorylist_opt_id2[]     This corresponds to the ID field.
@inventorylist_opt_id3[]
@inventorylist_opt_id4[]
@inventorylist_opt_id5[]
@inventorylist_opt_val1[]  - These five arrays contain option value for items.
@inventorylist_opt_val2[]
@inventorylist_opt_val3[]
@inventorylist_opt_val4[]
@inventorylist_opt_val5[]
@inventorylist_opt_param1[]- These five arrays contain option param for items.
@inventorylist_opt_param2[]  This field is unused?
@inventorylist_opt_param3[]
@inventorylist_opt_param4[]
@inventorylist_opt_param5[]
@inventorylist_expire[]    - expire time (Unix time stamp). 0 means never
                             expires.
@inventorylist_bound       - whether it is an account bounded item or not.
@inventorylist_count       - the number of items in these lists.
@inventorylist_favorite    - (unused?)
@inventorylist_idx         - inventory index of the item

This could be handy to save/restore a character's inventory, since no
other command returns such a complete set of data, and could also be the
only way to correctly handle an NPC trader for carded and named items who
could resell them - since NPC objects cannot own items, so they have to
store item data in variables and recreate the items.

Notice that the variables this command generates are all temporary,
attached to the character, and integer.

Be sure to use @inventorylist_count to go through these arrays, and not
getarraysize(), because the arrays are not automatically cleared between
runs of getinventorylist().

---------------------------------------

*instanceOwner(instanceID)

This command will return the account ID of the owner of the instance.
It'll return "0" for global instances, or if the instance does not exist.
Party and Guild instances will return the ID, but will not return the type.

---------------------------------------

*aggravate(GID)

Makes the object with game ID "GID", which must be a monster, to attack the
attached player, and inflicts "provoked" status ailment.
Provoked monsters will not, normally, change their attack target.
Returns -1 if GID is invalid, returns nothing otherwise.

---------------------------------------

*calcdmg(src, target, type)

Calculates how much damage src would deal in target in an attack of type [type].
[type] must be either BF_WEAPON or BF_MAGIC.
If target passed is 0 or negative, target (for defense) will not be considered.

Returns adjusted damage in absolute numbers.

---------------------------------------

*harm(guid, raw_damage, {type{, element{, source}}})

Note: if source is not passed, it'll default to attached player (if any), or use
the [guid] (target GID) as source.

Causes damage to GID, taking everything in account.
Type must be either BF_WEAPON, BF_MAGIC or BF_MISC (default).
Element defaults to ELE_NEUTRAL.
Note that BF_MISC disregards defense, but not element.

Returns total amount of damage inflicted.

---------------------------------------
*resetrng()

Resets the server PRNG seed and reinitializes the random number generator status.

---------------------------------------
*getskillname(skill_id)

Returns the name for the skill identified by its ID.

---------------------------------------
*bgnew(map_name, x, y, logout_ev, death_ev)

Creates a new Battle Ground Team, where players will respawn in map_name (x, y).
logout_ev and death_ev must be script label run on logout and death,respectively.
map_name must contain a "-" sign.

Should be used in conjuction of bgjoin().
The server will NOT distinguish the battle ground from teams created this way.
use bg_get_data to check how many players are assigned to the group ID.
Groups created this way additionally are never deleted.

Returns 0 if failed, otherwise, returns the Battle Ground Group ID.

---------------------------------------
*bgjoin(bgId, map_name, x, y{, accountID})

Forces player identified by accountID (or attached player if not provided) to
join Battle Grounds Group bgId, and teleports them to map_name (x,y).

Player will remain in the group until bg_leave() is used.
While in a battle ground group, they cannot attack nor be attacked by other
members from same group, or group monsters summoned via bg_monster, while they'll
be vulnerable to PVP from players of opposite groups.

Returns 0 if failed and 1 on success.

---------------------------------------

*npc_duplicate("<source_npc_name>", "<new_npc_name>", "<mapname>", <map_x>, <map_y>{, <sprite_id>, <dir>{, <map_xs>, <map_ys>}});

Duplicate any existing NPC based on <source_npc_name>.
Return 1 on success, 0 if failed.

---------------------------------------

*npc_duplicate_remove({"<npc_name>"});

Remove any duplicated NPC from source NPC or depend on <npc_name> if specified.
If <npc_name> are source NPC, it will only remove all duplicated NPC.
It cannot remove itself.
Return 1 on success, 0 if failed.

---------------------------------------
*apicall(code, json)

Sends to Moubootaur Legends' API a [json] encoded string for [code] operation.
Requires server to have been compiled and running with PYLOG support.
If the API is not running, message will be queued and delivered when API comes
online.

Returns 0 if compiled without PYLOG support, 1 otherwise.

---------------------------------------
*apiasync(message_id, contents)

Sends to logmaster.py [message_id] with [contents]. The message id is configured
by the python script and must include SQL, SAD* and SQLRUN.
Requires server to have been compiled and running with PYLOG support.

Returns 0 if compiled without PYLOG support, 1 otherwise.

