# General Balance Formulas for TMW2

As usual, general balance formulasare as generic as they could. Tweak as needed.

I repeat, **TWEAK AS NEEDED**. Just don't go too far from them or you'll end up
with a broken balance.

## Monster Experience Gain

* Level 1: total 1 mobs individual 3 xp (1.450)
* Level 2: total 3 mobs individual 5 xp (1.450)
* Level 3: total 8 mobs individual 5 xp (1.450)
* Level 4: total 16 mobs individual 4 xp (1.450)
* Level 5: total 26 mobs individual 7 xp (1.450)
* Level 6: total 39 mobs individual 8 xp (1.450)
* Level 7: total 56 mobs individual 9 xp (1.450)
* Level 8: total 77 mobs individual 9 xp (1.450)
* Level 9: total 101 mobs individual 10 xp (1.450)
* Level 10: total 129 mobs individual 11 xp (1.450)
* Level 20: total 667 mobs individual 21 xp (1.450)
* Level 30: total 1767 mobs individual 57 xp (1.450)
* Level 40: total 3597 mobs individual 148 xp (1.475)
* Level 50: total 6724 mobs individual 295 xp (1.525)
* Level 60: total 11871 mobs individual 579 xp (1.575)
* Level 70: total 20071 mobs individual 952 xp (1.625)
* Level 80: total 32389 mobs individual 1783 xp (1.650)
* Level 90: total 47806 mobs individual 3635 xp (1.650)
* Level 100: total 66308 mobs individual 4976 xp (1.650)

Formula:
```py
    hxp=exp[lv]
    if not mb.boss:
        if lv > 35:
            fx=1.45+((lv-35)/200.0)
        else:
            fx=1.45
        if fx > 1.65:
            fx=1.65

        # Aggressive, assistant and fast mobs yield more exp
        # Slow monsters yield less exp, because they're easier to kill on any playstyle
        if magr:
            fx-=0.1
        if mass:
            fx-=0.08
        if mv < 200:
            fx-=0.02
        if mv > 500:
            fx+=0.02

        # 1 ^ anything = 1, so we need a better rule
        if lv != 1:
            lxp=exp[lv-1]/(lv**fx)
        else:
            lxp=3 # 3~5 is fine
    # Boss are BOSS. It gives roughly 0.1% from lvlup xp.
    else:
        lxp=exp[lv]/1000
```

## Monster HP

Please take in account the weapon damage formulas.

```py
    # HP: Each 10 levels give you 100 extra weapon damage
    # I expect from 6 to 14 hits to kill
    # You deliver in average two hits per second
    # Mobs deliver one hit each four seconds (and that's 1hand advantage)
    lhp=lv*20*6+lv*(vi/100)
    hhp=lv*20*14+lv*(vi/100)
    if mb.boss:
        lhp*=1.4
        hhp*=1.8
    if "slime" in mb.name.lower():
        lhp*=0.6
        hhp*=0.6
    if attackrange > 1:
        lhp*=0.9
        hhp*=0.9
```

## Monster Attack

This one is tricky because it was calculated without defense.
But HPTable formula seems to be doing the trick thus far.

```py
    # HPTable Formula: 400+(x*50)
    # MASTERED
    lat=(lv*40+lv**1.2+lv*(st/100))/ar
    hat=(lv*40+lv**1.5+lv*(st/100))/ar
    # Special Cases
    if mb.boss:
        lat*=1.2
        hat*=1.4
    if "slime" in mb.name.lower():
        lat*=0.3
        hat*=0.3
    # Attack is DPS. So we must adjust to be a damage per second
    lat*=(ad/1872)
    hat*=(ad/1872)
    # Formula is not reliable
    lat/=3
    hat/=3
    # Consider Aggressive and Assist mobs
    if magr:
        lat*=0.78
        hat*=0.78
    if mass:
        lat*=0.89
        hat*=0.89
```

## Weapon Damage

This should be capped at 11/level because that's how mobHP formula works.

### Melee Weapons
```
    1 hand weapon: Level * 8
    2 hand weapon: Level * 10
    Crafting items: Critical Damage +30%
    2 hand weapon: Dex/Agi -(Lvl/10), Def -(Lvl*0.8), Crit +(Lvl/5)
    HH/Rare/Craft/Lucky bonus: Multiplier +1 (except Kanabo & Centaur Spear)
```

Please keep in mind that a critical hit disregards flee rate (but not blocking).
Critical hits also disregards defense. This means that, while 2 hand users
suffer from less dex, their additional critical rate makes up for that.
With the added bonus that critical ignores mob evasion!

TL;DR - Two hand swords can hit Tortugas. Critical here is different.
Note: Perfect Blocking (from shields) is absolute. 2h swords have no power against.

Centaur Spear have increased range, so damage is not nominal.

### Magic Weapons

With the new Magic System, MATK behaves in a similar fashion to regular ATK.
It is not an exact copy, however.

```
    Magic Attack = Two Hand Sword ATK
    Each 2 levels, they add 1 to maximum SP pool.
    
    There are no other bonus atm, but we have about 30 available to use.
```

### Ranged Weapons

They are divided in two grand groups: Arrow-based and Bullet-based.

```
    Redesign for bows will follow the following formula:
    lv×(8+(lv÷100))
```
In other words: multiplier is `8 + (level/100)`
So for a level 60 bow, multiplier will be 8.6

```
    Rifles: 2 hand sword damage
    Revolvers: 4/5 from 1 hand sword damage
    Shotgun: 3/5 from 1 hand sword damage
    Gatling Gun: No damage applied
```

Firearms need a total rewrite. I haven't tested them properly either, just like magic.

## Ammo Box Prices

The arrow prices in the ammo boxes is:

```
    300+200×0,05×(dmg-10)
```

This way, Tolchi Arrow (weakest) cost 300 Gp. You can get 200 GP by selling them.
This shall be enough to prevent profit from buying packs and selling arrows.

Each arrow cost 0,05 GP the damage (minus Tolchi Arrow). However, very powerful
arrows might have special effects or need to have less damage to don't overpower.

## Quivers Status

Maximum quiver attack speed bonus is 35% because otherwise it gets faster than
melee and this is strictly forbidden.

Required agility is 70% from base level - if you are distributing your points
evenly across agility and dexterity, this shouldn't be a problem.

## Armor Defense Distribution

![Defense Scaling in TMW2](server/formulas/Defense.png)

Blue line is DEFENSE, Red line is MAGIC DEFENSE.

As you can see, defense use a non-linear scale, ultimately capping around 10.0%
damage received as you near absurd amounts of defense (eg. a million).

Therefore, we used 350 defense (which means 40% damage reduction) as reference for
level 100 and baked the following formula:

```
Def=(Lv**1.255)*2.5
Def=Def*350/810.0
```

You'll notice it won't give exactly 350 defense at level 100 - it gives a bit more.
That's because rounding.

That is TOTAL LEVEL MAXIMUM DEFENSE. When reaching higher level equipment, consider
giving VIT DEF (aka. Raw Damage Reduction or Hard DEF).

Take in mind that **critical attacks disregard defense**, and therefore, melee users,
specially tankers, will really need critical defense.

The level maximum defense is divided roughly as follows, remembering that attribute
*Heavy* stands for armor which *lowers movement speed*.

```
Heavy Armor - 40%
Light Armor - 30%

Heavy Helmet - 15%
Light Helmet - 10%

Leggings - 10%

Shields - 25%
Boots - 8%
Gloves - 7%
```

This totals to 105% when using heavy armor with shield. Archers, mages, two handed
weapon users will therefore have significant less defense.

In general, archers and mages cannot/shouldn't use heavy armor or shields, so they have
only 60% defense from planned.

Users with two hand swords can use the heavy armor (and probably will use it, too).
They will enjoy 75% defense.

If for some reason a melee fighter decide to don't use the heavy armor because
speed, they'll still have 85% defense from the reference, meaning that not using
a shield is worse than not using heavy armor.

NOTE: If an armor piece raise magic attack, the defense for said piece is HALVED!

| Lv | Damage Taken | Nominal Defense |
|--|--------------|----|
|0 | 100% | 0 |
| 10 | 95.83% | 19 |
| 20 | 90.65% | 46 |
| 30 | 85.45% | 77 |
| 40 | 80.49% | 110 |
| 50 | 75.88% | 146 |
| 60 | 71.63% | 184 |
| 70 | 67.75% | 223 |
| 80 | 64.20% | 264 |
| 90 | 60.97% | 306 |
| 100 | 58.03% | 349 |

### Hard Defense Distribution

Hard Defense, also known as Def2 or Vit Defense, is the absolute damage negation.
Let a Hard Defense of 2.

10 damage will become 8. 100 damage will become 98.

#### Chestplates

Magic Armor - NONE

Light Armor - +1 each 10 levels (10%)

Heavy Armor - +1 each 5 levels (20%)

Always rounded down.

#### Shields

Real Shields - +1 each 5 levels (20%)

### Helmets

* Magic Helmet - NONE
* Light Helmet - +1 each 10 levels (10%)
* Heavy Helmet - +1 each 5 levels (20%)

#### Leggings

+1 each 10 levels (10%)

#### Shoes

+1 each 10 levels (10%)

#### The General Formula

In general we get +1 dmg per level at monsters; 
that must be evenly split with heavy armor.

So shields + heavy armor + shoes + heavy helmet +
leggings = 1 defense per level

I've removed gloves from this because otherwise
it gets too specific; Gloves should use ASPD
instead of DEF2. Or even ignore level and rely
on special bonuses only.

### Equip HP Distribution

Monster attack causes +5 hp loss per level.
HP Table rule is:

begin at 350, add 50 HP/Lv.
Every 5 levels, decrement 1.15 HP gained.

So at level 32, HP is `350 + (50 * 5) + (48.85 * 5) + (47.7 * 5) + (46.55 * 5) + (45.4 * 5) + (44.25 * 5) + (43.1 * 2)` = 1849.95 HP

But it is always rounded down, so it is not 1850, but 1849 HP.
Formula carries over values. See tools/misc/hp_table.py

At level 100, the HP increment is of mere 27 points.
Equipment, thus, could play a major role of survivability of
high level players.

Equips will also use a similar table, but reversed logic.
It have no start value, and step is of 4 HP/lv - but it increases 1.15 HP
every 5 levels.

Gloves, again, are excluded. Their sole purpose is to increase ASPD or MATK.

The HP bonus is divided roughly as follows, remembering that attribute
*Heavy* stands for armor which *lowers movement speed*.

```
Heavy Armor - 30%
Light Armor - 20%
Magic Armor - 0%

Heavy Helmet - 20%
Light Helmet - 15%

Leggings - 10%

Shields - 35%
Boots - 5%
```

This totals to 100%.

### Equip HP Bonus Table
| | | | | | | | | | | |
|-|-|-|-|-|-|-|-|-|-|-|
| 0 | 4 | 8 | 12 | 16 | 21 | 26 | 31 | 36 | 41 |  Level 1 - 10 |
| 48 | 54 | 60 | 66 | 73 | 80 | 88 | 95 | 103 | 110 |  Level 11 - 20 |
| 119 | 127 | 136 | 144 | 153 | 163 | 172 | 182 | 192 | 202 |  Level 21 - 30 |
| 213 | 224 | 234 | 245 | 256 | 268 | 280 | 292 | 304 | 317 |  Level 31 - 40 |
| 330 | 343 | 356 | 369 | 383 | 397 | 411 | 426 | 440 | 454 |  Level 41 - 50 |
| 470 | 485 | 501 | 516 | 532 | 548 | 565 | 582 | 598 | 615 |  Level 51 - 60 |
| 633 | 651 | 668 | 686 | 704 | 723 | 742 | 761 | 780 | 799 |  Level 61 - 70 |
| 819 | 839 | 859 | 879 | 899 | 921 | 942 | 963 | 984 | 1006 |  Level 71 - 80 |
| 1028 | 1050 | 1073 | 1095 | 1118 | 1141 | 1165 | 1188 | 1212 | 1235 |  Level 81 - 90 |
| 1260 | 1285 | 1309 | 1334 | 1359 | 1385 | 1410 | 1436 | 1462 | 1488 |  Level 91 - 100 |
| 1515 | 1542 | 1569 | 1596 | 1623 | 1651 | 1679 | 1707 | 1736 | 1764 |  Level 101 - 110 |
| 1793 | 1822 | 1852 | 1881 | 1910 | 1941 | 1971 | 2002 | 2032 | 2063 |  Level 111 - 120 |
| 2094 | 2126 | 2157 | 2189 | 2221 | 2253 | 2286 | 2319 | 2352 | 2384 |  Level 121 - 130 |
| 2418 | 2452 | 2486 | 2520 | 2554 | 2589 | 2624 | 2659 | 2694 | 2729 |  Level 131 - 140 |
| 2765 | 2801 | 2838 | 2874 | 2910 | 2947 | 2985 | 3022 | 3059 | 3097 |  Level 141 - 150 |
| 3135 | 3174 | 3212 | 3251 | 3289 | 3329 | 3369 | 3408 | 3448 | 3488 |  Level 151 - 160 |

## Attack Speed Bonus

Only gloves have Aspd bonus.

Formula: Level / 5

Once again, does not apply to magic.


