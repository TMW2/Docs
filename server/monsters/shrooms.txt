ID	        1043	        1042	        1060	    1176	        1069
Name	    Poison Spiky Mushroom	Red Mushroom	ForesT Mushroom	Wicked Mushroom	Moonshroom
lv	        22	            23	            54	        61	            80
HP	        2763	        4200	        6439	  8587	            11346
EXP	        28	            97	            406	        844	            1976
JEXP	    8	            10	            20	        30	            39
AttackRange	1	            1	            1	        1	            4
AttackRange	270,3	        361,393	        764,856	    458,518	        285,329
Def	        19	            26	            49	        55	            64
Mdef	    11	            15	            36	        40	            72
Stats					
Str	        9	            12	            24	        32	            41
Agi	        18	            22	            35	        40	            55
Vit	        17	            26	            49	        53	            72
Int	        1	            5	            9	        15	            50
Dex	        18	            30	            45	        51	            66
Luk	        11	            15	            22	        27	            33
ViewRage	1	            7	            7	        12	            7
ChaseRange	12	            12	            12	        12	            11
Race	    2	            2	            2	        2	            2
Element	    5,1	            3,1	            5,1	        7,1	            5,3
MoveSpeed	800	            780	            760	        740	            720
AttackDelay	1572	        1872	        2372	    1172	        1772
Drops					
MushroomSpore650	        750	            850	        950	            1150
Plushroom	700,400	        400	            800	        2200	        600
Chagashroom	                800	            400	        1400,800	    300
SmallMushroom400	        450	            500	        550	            600
HardSpike	400		                        400	        550	
Powder				                                    700	            15
B drop		                50	            150	        600	            350, 50
gems			                            3	        11	            5
VRD	        1	            1	            1	        2	            2
VRD ID	    3002	        3019	        3003	    3007	        3008
VRD Name	Shroom Hat	    Mush Hat	    Forest Shroom Hat	Wicked Shroom Hat	Moonshroom Hat
Buy	        21000	        22000	        23000	    24000	        25000
Sell	    10000	        10000	        10000	    10000	        10000
Weigh	    240	            290	            320	        420	            250
Def	        23	            26	            33	        42	            51
EquipLv	    30	            40	            50	        60	            70
bStr	    2	            3		                    5	
bInt		                1	            1		                    7
bDex				                                    1	
