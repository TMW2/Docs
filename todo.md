# TMW2: Moubootaur Legends
## Jesusalva's ToDo List

> This is my personal TODO list and ML task hub. Keep in mind I don't use ML's
> money for myself. If you want to buy me coffee to keep developing, it must be
> done [on my own Buy me a Coffee](https://www.buymeacoffee.com/saves) instead.

## Moubootaur Legends Requests

* [x] Fix homun skills
  * [ ] Verify if Homun Passives still work
* [ ] Politic Expansion
  * [x] Town Event System (invest town GP)
  * [x] Town Donation Box
  * [x] Set bounties for sieges with town GP
  * [ ] Deploy barricades - next town siege easier
  * [x] Guard Training - next town siege gives extra map EXP
  * [ ] Declare war against another town
* [ ] Alchemy: Add ascending recipes (specially ores)
* [ ] Intense Beard: Improve dialogs
* [ ] quests.xml - Add Blossom's and Thorn's Requests
* [x] Transmutation/Transmigration Interface
    * [ ] Boia/Bola Subsystem (D)
    * [ ] Evol Potions Subsystem (DD)
    * [ ] Water Subsystem (A)
    * [ ] Arrow Subsystem (A)
    * [x] Raw Coal Subsystem (-)
    * [ ] Gempowder Subsystem (R)
    * [ ] Herb Subsystem (R)
    * A: Ascending, D: Descending, DD: Double Descending, R: Random
* [ ] Zambacutou Quest (and replace the temp way to get it)
* [ ] Deal with unreleased consumables (remove or merge)
    * [x] Apana Monster and Cake
        * Candor Clothes, 150ms WLK, 5s ASPD, 1k DMG, low HP, aggro
    * [ ] JellyBeans
    * [ ] Pumpkin Juice
    * [ ] Pumpkin Seed
    * [ ] Big Pumpkin Seed
    * [x] Pumpkin Lollipop
    * [ ] Love Lollipop
    * [ ] Manapop
    * Find more unreleased consumables server-side
    * Find more unreleased consumables client-side
* [ ] Assassin Archery
    * [x] Raise all bow range in +1 (or add the add_range passive skill)
    * [ ] Verify Explosive Arrow
    * [ ] Replace Bone Arrow with Explosive Arrow (Back-forth)
* [ ] Tent/Campfire
* [ ] PVP System and anti-PVP items
    * [x] Honor Score
    * [ ] PvP Imperial Arena
    * Near Frostia, to elect the PVP King, see #admin
* [ ] Balance Concerns
* [ ] Auto-complete Sewer quest when entering town from sewers
* [x] Guild Badge System
    * [x] Guild Quest
    * [ ] Guild Bounty (GM)
    * [ ] Use variables instead of items?
        * What about guild-less bounty-hunters, then?
    * [x] Guild Badge Shop
    * [x] Max dungeon attempts: Guild Level?
    * [x] Badge Reward based on Guild Level? (1 badge/level)?
    * [ ] Guild - Warp object
    * [x] Guild - Kamelot Castle
    * [ ] Guild - GvG
* [-] Ancient Blueprints on Ancient Desert (blame @Saulc I guess)
* [x] Main Story 6 and 7
    * [x] LoF Townhall
    * [ ] Lalica Arch
        * [ ] The Temple
        * [ ] Her Residence
        * [ ] The Showdown
        * [ ] Misc. Scripts
    * [ ] Airship Graphics (Bottom area, unless it is Tiled building)
    * [ ] Airship Landing Area
    * [x] Fortress Island
    * [ ] Intro Cutscene
    * [ ] Ancient Legacy (PS Ultimate)
    * [ ] Random Treasure and Abandoned Houses
    * [ ] Kingdom War System (reoutfit for GS-3)
    * [x] Impregnable Fortress Boss Room (and the boss fight)
    * [ ] Final Boss and Cutscenes
* [ ] Magic System version 3
    * [ ] Joyplim
    * [ ] Skill Unit to script System
    * [ ] Shadowclone (?)
    * [ ] Revive, Resurrect, Redemption
      * [x] Resurrect
      * [ ] Redemption
    * [ ] Sanctum, Imperial Seal
    * [x] Brawling, Bear Strike, All In One
    * [ ] Sacrifice
    * [ ] Scripture
      * [x] Chanting
    * [ ] Liberation Strike
    * [ ] Weapon Elemental Adjustment Skills
    * [x] Earth Skill Class
    * [ ] Assassinate
    * [ ] Arrow of: Devour, Urges, Ultima, Execution, Shatter,
          Advantage, Awakening, Flames
    * [ ] Horizontal, Diagonal, Vertical Slash, Stab, Grand Blast
    * [ ] NPC: Princess Of Night (Quest: Night in Red)
* [ ] Fishing v2: Open Seas
    * [ ] Rowboat
    * [ ] Mechanics
    * [ ] NPC to allow it
* [x] Public Lightining System
    * [x] Add 'town light' NPCs
    * [x] Enable/Disable them along day/night announces
        * [x] Candor
        * [x] Tulimshar
        * [ ] Halinarzo
        * [x] Hurnscald
        * [ ] LoF Village
        * [ ] Lilit Island
        * [x] Nivalis
        * [ ] Frostia
        * [ ] Aethyr
        * [ ] World's Edge Village
* [ ] Setzer
    * [ ] Monster Potion - dyed, special avatar
    * [ ] Monster Potion Item
    * [ ] Monster Potion Making at Nivalis
    * [x] Short Sword *somewhere* besides HH Store.
    * [ ] Pour the potion over the short sword... (How?)
    * [ ] Zambacutou
* [ ] Lilit
    * [ ] Real Estate
    * [x] Leather Trousers
* [ ] Mechanics
    * [x] Warp Crystals
    * [x] town.txt - Users coming back from death might need LOCATION$ reset
    * [ ] NPC_<Ele>ATTACK to all relevant monsters (overpopulating mob skill db)
* [ ] Monsters
    * [ ] Spider
    * [ ] Flowers
    * [ ] Duck King
    * [ ] Gatekeeper
    * [ ] Hydra Cat
    * Concepts:
    * [ ] Scorpion King
    * [ ] Mechanical Beigns (Assault Weapons, (Mobile) Fortifications, Siege Crossbows...)
    * [ ] Sea Monsters (Hydra, Tentacles, Octopus, Sea Snake, Tritans, Nagas, Merfolk, etc.)
    * [ ] Mimics?
    * [ ] Generic Cave Monster (strange creatures without counter-part on real world)
    * [ ] Harpies, Eagles, Birds, Gryphons, etc.
    * [ ] Magical beings: Elementals (Fire elemental, Water elemental, etc.), Summon
          Gates, Homunculuses, Woses, Golems (small, big, fat, thin, with swords, with lazers, etc.) ...
    * [ ] Insects and Insectoids (eg. ants, bees, annoying flies and swarms, lizards,
          Alligators, etc.)
    * [ ] Animals like deer, pigeon, pigs, boars, bears, foxes, goats, horses,
          monkeys, lions, tigers, elephants, etc.
    * [ ] Ninjas, Gladiators, etc. for the PvP arena - It should work, if we only
          have one player for the PVP Duel, no need to cancel the fight - just spawn
          a few Gladiator monsters and enjoy :>
    * [ ] Dragons (Red Dragon, Smoke Dragon, etc. - Green Dragons being the weakest)
        * [ ] Wyverns and Drakes can be done, too
    * [ ] Other mythical stuff like goblins, kobolds, gnomes, gargoyles, ogres,
          cyclops, dwarves, etc.
    * [ ] Monsters using the sprite builder (elves, redys, orcs, etc.)

* [ ] Other
    * [ ] Katzei Family
        * [ ] Black Cat Boots ( serverdata#47 )
        * [ ] Cat Ears ( serverdata#47 and LOW PRIORITY )
    * [ ] Block @spawn of Moubootaur or Monster King
    * [ ] Automatic Treasure Chest monster spawn in dungeons (maybe with an event so
          previous spawns are killed each cycle, and population is kept roughly based
          on player count) - Or mimic :> Feeling lucky? :>
    * [ ] Traps, poison makers for poison arrows: Buy common Arrow, and ask a Poison
          Maker Player to upgrade them! (Make normal arrows cheaper, too)

* [ ] Experience Orb items
    * Treasure chests drop those?
* [ ] "Impossible" tasks
    * [ ] gmauth: Discord/IRC
    * [ ] Rain (Weather) should have an effect ingame
*  Party Dungeon Second Floor
    * Use setcells() and switches to close the wings
    * Add a step portal on the bottom of each wing. Player must be standing on it.
    * If a player is standing in every portal, all players get warped.
    A way to warp more is to everyone pile up at one portal, or to divide in equal-size
    teams
    *  Third floor, I don't care anymore at this point
    *  Release it!
* [ ] Extend Khafar
* [ ] Extend Agostine
* [ ] Fix Zombie Nachos effect
* [ ] Add Sparkly Mint Donut: ATK-or-ASPD?
* [ ] Add cooking recipes with potato, carrot, orange cupcake, chocolate bar
* [ ] Add pie and cake to cooking? Maybe all food cakes are pies?
* [ ] Pyndragon Extended Weapon Selection
    * [x] Lightsaber: Fast 2 hand staff (SETZER speed)
    * [x] He probably will inheirt Setzer as well
    * [x] Single Handed Sword already specialized in splash (Whip)
    * [ ] Techno-Wands specialized in raw attack speed and power
      * Knuckle? Instrument? Staff? Rod? Zapper? Spirit? Doll? Tome?
      * Will it even work with our int-based system?
    * [x] Power-Wand which autocasts lightning bolts (but is SLOW and eat mana)
      * [ ] Fix & release the Scepter of Thunder
    * [x] Thief's Sword, which steal items and may even collect loot! (Maybe)
      * [ ] Fix & release the Kunai
* [ ] Lalica the Witch (reward: Hat)
* [ ] Graveyard Island

----

# Archived Old Still Relevant ToDo lists

## Mapping Requests
* [x] Katzei the Cat Cave
* [ ] Heroes Hold final levels
* [ ] Racing Maze
    * [ ] For Monsters
    * [ ] For Players and NPCs

## Game Lore
* [x] Next Main Storyline movement
    * Monster King and the fragment hunt: Script control that
    * [x] Main Continent must be finished and more mobs must be added before going to
    his Stronghold and attempt to take it down - design so a team of five players
    level 109 with Savior Set can handle the task.
* [x] Next Player Storyline movement
    * We'll start using Karma System for real, and give you the three legacies based
    on that and affected by player choice.
    * Next movements are building Karma points, using main storyline frame.
    * [x] Lua is a good NPC if you like the "open world" style, but add the guard NPC...
        * [x] And then, use this Guard NPC to sort player priorities
* [ ] Next City Development
    * Nivalis still needs to be finished with Angela and Blue Sage.
    * This will move city development to Frostia and Lilit next.

### Real Estate System
* [x] Add a house in LoF Village
* [x] Mirror that house multiple times
* [x] Add a house in Halinarzo too (cheapest ingame)
* [x] The castle in the Road
* [ ] Fairy house
* [x] Apartment Building
* [x] Sanitize rent prices

### Dungeon Development
* [ ] Dungeon Fishing
    * Add more spots
    * Legendary Savior Set Blueprints
* [ ] We need a minecart in some dungeon (travel between cave chambers) in a Party Dungeon.
    * Or we could make it like Rush Game. Eh, board games are tricky.
* [ ] Heroes Hold
    * Traps should cause status effects at random too (these need client-data patch)
    * Add the missing levels for Crazyfefe if needed
* [ ] Grand Race
    * Objective is to reach first the other side of a maze - you're running against
    players AND NPCs (which have a set of random pathes they'll take). All equipment
    is allowed and skills are all green. GMs cannot participate.
    * This means if the race is hourly, even if nobody else is interested,
    there still are NPCs running and you may lose to them
    * You could bet Casino Coins too :3 Bet on your friend! Get more Casino Coins!

### LILIT, THE FAIRY ISLAND
* Quests and NPCs
    * [ ] Daily for Mountain Snake Egg
    * [ ] Permanent - Armor - for 40x Mountain Snake Skin
    * [ ] Fairy Pet - see Horcrux in BR for reference (haaaaaaaard, like Crazyfefe likes)
    * [x] Grand Hunter Quest - Mountain Snake (might take it while doing other quests)
    * [ ] Quest with Mountain Snake Tongue and Dragon Scales I guess?
    * [x] Leather Pants
* [ ] Lilit Central Tree
    * Player groups (lv 40) might want an audience with Lilit, thus, climing the tree.
    * They have to climb the stairs (ie. move upwards).
    Mobs are created as players progress. The mob is aggressive.
    * If players reach the top, they'll be brought to Lilit audience room.
    * [ ] Lilit challenge players to the Snake Pit if they want (like Crazyfefe Cave,
        but monster is fixed - Mountain Snake - with 1 extra snake every turn)
    Must kill all to advance round, and there's a reward at end (5+players*2 rounds?)
    It is a PARTY INSTANCE with warpparty().
    * [x] There's also a General there. So you can challenge the Yetifly.
    * Real Estate booking in Lilit is managed here, of course.
    * Diplomacy is welcome

### FROSTIA, THE FROZEN KINGDOM
* :questionmark: More elves quests so you can enter while suffering racism.
    * Cindy quest should not be a pre-requisite.
* [x] Assassin Set quests are ready - just copy paste
* [ ] Red Stockings
* [x] AF King Arthur
* [x] Replace Desert Bow by Elfic Bow and make it available.
* [ ] Challenge Frostia King to a duel with any PARTY. Causes a server cooldown.
    * No reward is provided. In other words, killing the king reward is suffice.
    * Or actually, we might give players some solid EXP boost, like a true boss.
* [x] Other and any level 70+ content when appliable.

### Other places
* [ ] Redy's Volcano?
* [ ] Oceania?
* [ ] Orc Nomad Village? (Barbarians?)
  * Modanung's Savannah?

#### Other Quests
* [ ] Inspector Quest (reward: Inspector Hat)
* [ ] Rock-Paper-Scissors (Halinarzo, probably casino)

#### Other items
* [ ] Monster Potion
* [ ] Setzer background story
* [ ] Fate's Potion

##### Missing Level 40~70 Equipment
* [ ] Setzer
    * Must learn about monster potion on Khafar
    * Monster potion is done at Nivalis Potion Shop
    * Must learn Setzer's Backstory «WHERE? WHICH ONE?»
    * Can be craft «WHERE? BY YOURSELF?»
* [ ] Golden 4 Leaf Amulet
* [ ] Platinum 4 Leaf Amulet
    * Requires (previous 4l amulet) and a real 4 leaf clover this time (besides platyna)
    * Supreme boost, finally
* There's stuff on Art Repo which wasn't taken in account.
    * Lack of notes like this one means lack of plans properly recorded somewhere.

#### Minor
* [ ] Treasure Chests animation
* [ ] Add story books to Halinarzo Library
* [x] Gemini Assassins Quest
    * [ ] Isbamuth
* [ ] Golden and Titan monsters
    * Stronger variation of common monsters with more rewards/harder to slay.
    * Golden is focused on drops, and Titan is focused on EXP.
* [ ] Discuss with Saulc about Nahrec the Heavy Armor Maker and about Warlord set
    * Bring the second helmet from Warlord Set from TMW-BR too, very good quest
* [ ] We could use real money...

