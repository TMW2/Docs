## Translation Roadmap

They're separate by domains, in priority order. It's expected to be translated sequentially.

`#F00` means not done\
`#FF0` means partial translation (> 60%)\
`#0F0` means complete (> 90% + reviewed)

"English" is for proof-reading, so `#FF0` means not proofreading not done.

|  Domain  | English | Portuguese | Spanish | German | French | Russian |
| -------- | ------- | ---------- | ------- | ------ | ------ | ------- |
| Nard     | `#FF0`  | `#0F0`     | `#0F0`  | `#0F0` | `#FF0` | `#FF0`  |
| Candor   | `#FF0`  | `#0F0`     | `#0F0`  | `#FF0` | `#F00` | `#FF0`  |
| Tulimshar| `#FF0`  | `#0F0`     | `#FF0`  | `#FF0` | `#F00` | `#FF0`  |
| Databases| `#FF0`  | `#FF0`     | `#FF0`  | `#FF0` | `#F00` | `#F00`  |
| Hurnscald| `#FF0`  | `#FF0`     | `#F00`  | `#FF0` | `#F00` | `#F00`  |
| Halinarzo| `#FF0`  | `#FF0`     | `#FF0`  | `#F00` | `#F00` | `#F00`  |
| LoF      | `#FF0`  | `#FF0`     | `#F00`  | `#FF0` | `#F00` | `#F00`  |
| Canyon   | `#FF0`  | `#FF0`     | `#FF0`  | `#F00` | `#F00` | `#F00`  |
| Academy  | `#FF0`  | `#F00`     | `#F00`  | `#FF0` | `#F00` | `#F00`  |
| Kaizei   | `#FF0`  | `#FF0`     | `#F00`  | `#FF0` | `#F00` | `#FF0`  |
| Events   | `#FF0`  | `#F00`     | `#F00`  | `#F00` | `#F00` | `#F00`  |
| Frostia  | `#FF0`  | `#FF0`     | `#F00`  | `#FF0` | `#F00` | `#F00`  |
| Special  | `#FF0`  | `#F00`     | `#F00`  | `#F00` | `#F00` | `#F00`  |
| Other    | `#FF0`  | `#F00`     | `#F00`  | `#F00` | `#F00` | `#F00`  |
| Kamelot  | `#FF0`  | `#FF0`     | `#F00`  | `#FF0` | `#F00` | `#F00`  |
| Fortress | `#FF0`  | `#F00`     | `#F00`  | `#F00` | `#F00` | `#F00`  |
| System   | `#FF0`  | `#F00`     | `#F00`  | `#FF0` | `#F00` | `#F00`  |

You can use [L'Edit](https://git.themanaworld.org/ml/tools/-/tree/master/ledit) to speed up your work.\
ledit.py is a nice interface integrated with MTL to speed up your translation.\
posplit.py will divide the big PO file from Transifex into one file per domain.\
You can then commission other people to work on the chunks or track completion from them.\
Then use `msgcat -o out.po domain*` before submitting to Transifex again.

Both require you to download the full PO file from [Transifex](https://app.transifex.com/arctic-games/moubootaur-legends/languages/).


If a language uses machine translation, it shall remain in `#FF0` until a human review and approves the translations.
-----
### Rough Roadmap

- 19.1: domain/Nard + domain/Candor  (1366 strings / 14k words)
- 19.3: domain/Tulimshar (1928 strings / 20k words)
- 19.4: 10% of domain/Database + domain/Hurnscald (1792 strings / 16k words)
- 19.5: 40% of domain/Database + domain/Halinarzo (1582 strings / 10k words)
- 19.6: domain/LoF (1414 strings / 15k words)
- 19.7: 50% of domain/Database + domain/Canyon (1739 strings / 11k words)
- 19.8: domain/Academy + domain/Kaizei (1289 strings / 14k words)
- 19.9: domain/Events + doamin/Frostia (1134 strings / 10k words)
- xx.x: domain/Special + domain/Other (1249 strings / 5k words)
- xx.y: domain/Kamelot + domain/Fortress + domain/System (977 strings / 8k words)
#### This roadmap has the string count of 2024-08-31 00:00 UTC
